<?php

require_once('lib/helper.php');
require_once('lib/enqueue-assets.php');
require_once('lib/sidebars.php');
require_once('lib/theme-support.php');

// tried this to get ACF to work with Gutenburg, but let down by the preview again.
//require_once('lib/acf_helper.php');

?>
