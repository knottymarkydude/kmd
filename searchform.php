<form role="search" method="get" class="l-search-form" action="<?php echo esc_url(home_url('/')) ?>">
  <label class="l-search-form__label">
    <span class="u-screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', '_themename' ) ?></span>
      <input type="search" class="l-search-form__field" name="s" placeholder="<?php echo esc_html_x( 'Search', 'placeholder', '_themename' ) ?>"
        value="<?php echo esc_attr(get_search_query()); ?>"/>
  </label>
  <button type ="submit" class="l-search-form__button">
    <span class="u-screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', '_themename' ) ?></span>
    <i class="fas fa-search" aria-hidden="true"></i>
  </button>
</form>
