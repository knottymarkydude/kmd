<?php
 /**
  * The main template file
  *
  * This is the most generic template file in a WordPress theme
  * It is used to display a page when nothing more specific matches a query.
  * E.g., it puts together the home page when no home.php file exists.
  *
  *  <pre>
  *   <?php var_dump($wp_query); ?>
  * </pre>
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package kmd
  */

 get_header();
 ?>

 <?php if (have_posts()) { ?>
     <?php while (have_posts()) { ?>
       <?php the_post(); ?>
         <h2>
           <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title() ?></a>
         </h2>
         <div>
           <?php kmd_post_meta(); ?>
         </div>
         <div>
           <?php the_excerpt(); ?>
         </div>
         <div>
           <?php kmd_readmore_link(); ?>
         </div>
         <?php the_posts_pagination(); ?>

       <?php } ?>
   <?php }else{ ?>
     <p><?php _e('Sorry no posts matched your criteria.', 'kmd') ?> </p>
   <?php } ?>

   <?php
   $comments = 3;

   printf(_n('One comment', '%s comments', $comments, 'kmd'), $comments);

   _x('Posts', 'noon', 'kmd');

   echo esc_html__( $text, $domain = 'default' );

   echo esc_html__( $text, $domain = 'default' )
   ?>

 <?php
 get_footer();
 ?>
