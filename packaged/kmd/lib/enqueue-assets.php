<?php

function kmd_assets() {
  wp_enqueue_style( 'kmd-stylesheet', get_template_directory_uri() . '/dist/assets/css/style.css',
  array(), '1.0.0', 'all' );

  //wp_enqueue_script('jquery');

  wp_enqueue_script( 'kmd-scripts', get_template_directory_uri() . '/dist/assets/js/bundle.js', array(), false, true );
}

add_action( 'wp_enqueue_scripts', 'kmd_assets');

function kmd_admin_assets() {
  wp_enqueue_style( 'kmd-admin-stylesheet', get_template_directory_uri() . '/dist/assets/css/admin.css',
  array(), '1.0.0', 'all' );

  wp_enqueue_style( 'kmd-admin-scripts', get_template_directory_uri() . '/dist/assets/css/admin.js',
  array(), '1.0.0', 'all' );
}

add_action( 'admin_enqueue_scripts', 'kmd_admin_assets');
