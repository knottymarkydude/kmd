// ---------------------------------------------------------
// Helpers
//
// Mixins and other essentials for global theme development.
//
// /sass/base/-helpers.scss
//
// ---------------------------------------------------------


// CSS Grid mixin with flexbox fallback for IE and Edge
// Usage: @include grid(12, 1, 200px, 400px, 1em, 1em);
@mixin grid($columns, $fr: 1, $min_height: 100px, $max_height: auto, $gutter_x: 16px, $gutter_y: 16px)


// Fontface
// Usage: @include fontFace('karla-regular', '../fonts/karla-regular', 400, normal);
@mixin fontFace($family, $src, $weight: normal, $style: normal)


// Background cover
// Usage: @background-cover();
@mixin background-cover()

// Background auto
@mixin background-auto()
Usage: @background-auto();

@mixin hiddentext()
Usage: @include hiddentext();

// Clearfix
// Usage: @include clearfix();
@mixin clearfix()

// Vertical Align
// Usage: @include vertical-align();
@mixin vertical-align

// Usage: @include horizontal-align();
@mixin horizontal-align

// Usage: @include centered();
@mixin centered

//@example scss - 5vw font size (with 50px fallback),minumum of 35px and maximum of 150px
// Usage: @include responsive-font(5vw, 35px, 150px, 50px);
@mixin responsive-font($responsive, $min, $max: false, $fallback: false)

// Usage: @include font-size(14px)
@mixin font-size($size)

// Usage @include font-smoothing(on);
@mixin font-smoothing($value: on)

// transition
.item {
    @include transition(padding, 1s, ease-in-out);
}
@mixin transition($transition-property, $transition-time, $method : ease)

.item {
    @include radius(20px);
}
@mixin radius ($radius-size)

// Usage:
.item {
    @include box-shadow(0, 1px, 3px, rgba(0, 0, 0, 0.15), true);
}
@mixin box-shadow($top, $left, $blur, $color, $inset: false)

// Usage:
// Assuming the parent element has position: relative;, these four
// properties will center a child element both horizontally and vertically inside
@include centerer();
@mixin centerer

// Flexbox display - for the flexbox wrapper
@include flexbox();
@mixin flexbox()

// The 'flex' shorthand
// - applies to: flex items
// <positive-number>, initial, auto, or none
@include flex(1 1 auto);
@mixin flex($values)

// Flex Flow Direction
// - applies to: flex containers
// row | row-reverse | column | column-reverse
@include flexflex-direction(row);
@mixin flex-direction($direction)

// Flex Line Wrapping
// - applies to: flex containers
// nowrap | wrap | wrap-reverse
@include flex-wrap(wrap);
@mixin flex-wrap($wrap)

// Display Order
// - applies to: flex items
// <integer>
@include flex-wrap(wrap);
@mixin order($val)

// Flex grow factor
// - applies to: flex items
// <number>
@include flex-grow(2);
@mixin flex-grow($grow)

// Flex shrink
// - applies to: flex item shrink factor
// <number>
@include flex-shrink(1);
@mixin flex-shrink($shrink)

// Flex basis
// - the initial main size of the flex item
// - applies to: flex items initial main size of the flex item
// <width>
@include flex-basis(200px);
@mixin flex-basis($width)

// Axis Alignment
// - applies to: flex containers
// flex-start | flex-end | center | space-between | space-around
@include justify-content
@mixin justify-content($justify)

// Packing Flex Lines
// - applies to: multi-line flex containers
// flex-start | flex-end | center | space-between | space-around | stretch
@include align-content(center);
@mixin align-content($align)

// Cross-axis Alignment
// - applies to: flex items
// auto | flex-start | flex-end | center | baseline | stretch
@include align-self(center);
@mixin align-self($align)
