var functions = {};

/***
To create a Org, we need check "Duplicated" first.
We consider the following 1 situation as "Duplicated"
1) org name contains the first word in input string

If falg is -1, we just return the query result and do nothing.

If "Duplicated" happen, we need check the flag
0: do nothing, just throw error
1: create a new org, DO NOT touch the old contact (like "duplicated does NOT happen")
2: update the old org. (BUT if we find more than 2 old org, we do nothing, just throw error like 0)
***/
functions.createOrg = function * (sfApi, args, flag) {
    var recordData = {
        Name: args.name,
        Website: args.website,
        Description: args.description,
        org_city__c: args.city,
        org_mailing_address__c: args.mailingAddress,
        org_province__c: args.province,
        org_postal__c: args.postal,
        org_country__c: args.country,
        Primary_Organization_Type__c: args.primaryOrgType,
        Secondary_Organization_Type__c: args.secondaryOrgType,
        Member__c: true,
    }

    //get first word in name
    var name = args.name.replace(/'/g, "\\'").split(" ")[0];
    if (! name) throw new Error(JSON.stringify({ type: 'ORG_NAME_EMPTY' }));

    //query org by org name
    var response = yield sfApi.queryRecords({
        soqlQuery: "SELECT id FROM Account WHERE RecordType.Name='Organization' AND Name LIKE '%" + name + "%'"
    });
    console.log(response.body.records.length, 'response.body.records.length......org...........');

    var foundRecords = [];
    for (let item of response.body.records) {
        foundRecords.push(item.Id);
    }

    if (flag === -1) return foundRecords;

    if (response.body.records.length) {
        //yes duplicated
        if (flag === 0) {
            throw new Error(JSON.stringify({ DuplicatedRecords: foundRecords }));
        }
        else if (flag === 1) {
            //create a new record
            var response = yield sfApi.createRecord({ type: 'Account', recordData: recordData });
            var orgId = response.body.id;
        }
        else if (flag === 2) {
            //update contact
            if (response.body.records.length > 1) {
                throw new Error(JSON.stringify({ DuplicatedRecords: foundRecords }));
            }

            var orgId = response.body.records[0].Id;
            var response = yield sfApi.updateRecord({
                type: 'Account',
                recordData: recordData,
                recordId: orgId
            });
        }
    }
    else {
        //no duplicated, create org
        var response = yield sfApi.createRecord({ type: 'Account', recordData: recordData });
        var orgId = response.body.id;
    }

    return orgId;
}


functions.getOrganizations = function * (sfApi, args) {

  //query org by org name
  var response = yield sfApi.queryRecords({
      soqlQuery: "SELECT id FROM Account WHERE RecordType.Name='Organization'"
  });
  console.log(response.body.records.length, 'response.body.records.length......org...........');


}

////////////////////////////////////////////////////////////////////////////////

module.exports = functions;
