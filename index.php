<?php
 /**
  * The main template file
  *
  * This is the most generic template file in a WordPress theme
  * It is used to display a page when nothing more specific matches a query.
  * E.g., it puts together the home page when no home.php file exists.
  *
  *  <pre>
  *   <?php var_dump($wp_query); ?>
  * </pre>
  *
  * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
  *
  * @package _themename
  */

 get_header();
 ?>
<div class="site">
  <div class="site-content">
    <div id="content" class="content-area">
      <main role="main" id="main" class="site-main">
        <section class="block">
          <div class="container">
            <?php get_template_part( 'template-parts/frags/loop'); ?>
          </div>
          <?php if(is_active_sidebar( 'primary-sidebar' )) { ?>
            <div class="container">
              <?php get_sidebar( "primary-sidebar" );?>
            </div>
          <?php } ?>
        </section>
      </main><!-- #main -->
    </div><!-- #content-area -->
</div><!-- #content -->
<?php
get_footer();
?>
</div><!-- site -->
