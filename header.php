<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset') ?> ">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index,follow">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head(); ?>
</head>

<body>
   <div id="page" class="site">
     <a class="skip-link screen-reader-text" href="#content"></a>
     <div class="site-content">

       <header role="banner" class="u-margin-bottom-40">
          <div class="l-header">
              <div class="l-container u-flex">
                <div class="fc-8c">
                  <div class="l-header__logo">
                    <a class="l-header__blogname" href="<?php echo esc_url(home_url( '/')); ?>">
                      <?php esc_html(bloginfo( 'name' )); ?></a>
                  </div>
                </div>
                <div class="fc-4c">
                  <?php get_search_form( true ); ?>
                </div>
              </div>
          </div>

       </header>

       <div class="content-area">
