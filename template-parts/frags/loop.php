<?php if (have_posts()) { ?>
    <?php while (have_posts()) { ?>
      <?php the_post(); ?>
        <?php get_template_part( 'template-parts/post/content' ); ?>
        <?php the_posts_pagination(); ?>
        <?php do_action( '_themename_after_pagination'); ?>
      <?php } ?>
  <?php }else{ ?>
    <?php get_template_part( 'template-parts/post/content', 'none' ); ?>
<?php } ?>

<?php
$comments = 3;

printf(_n('One comment', '%s comments', $comments, '_themename'), $comments);
?> 
