<?php
$footer_layout = '3,3,3,3';
$columns = explode(',', $footer_layout);
$footer_bg = 'dark';
?>

</div> <!-- content-area -->

<footer role="contentinfo" id="footer" class="site-footer">
		<div class="flex-container">
					 <!-- Flexbox 3 columns -->
					 <div class="fc-3c">
						 <div class="c-footer-wrapper-icon">
							 <img class="c-footer-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/dist/assets/gfx/Wellcome_Sanger_Institute_Logo_Landscape_Digital_RGB_Monotone_Black.svg" />
						 </div>
						 <div >
							 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ante lorem, pretium eu mi vel,
								 tincidunt efficitur magna.
							 </p>
						 </div>
					 </div><!-- End Flexbox 3 columns -->
					 <!-- Flexbox 2 columns -->
					 <div class="fc-2c c-divider-side-left">
						 <div class="c-footer-wrapper c-center-narrow">
						 <h6>Quick Links</h6>
							 <ul>
								 <li><a href="#">About</a></li>
								 <li><a href="#">Careers</a></li>
								 <li><a href="#">Study</a></li>
							 </ul>
					 </div>
					 </div><!-- End Flexbox 2 columns -->

					 <!-- Flexbox 5 columns -->
					 <div class="fc-5c c-divider-side-left">
						 <div class="c-center">
						 	<div class="flex-container">
								<div class="fc-4c">
									<a href="#">Sitemap</a>
								</div>
								<div class="fc-4c">
									<a href="#">Accessibility</a>
								</div>
								<div class="fc-4c">
									<a href="#">Terms & conditions</a>
								</div>
								<div class="fc-4c">
									<a href="#">Privacy & cookies</a>
								</div>
								<div class="fc-4c">
									<a href="#">Modern slavery statement</a>
								</div>
								<div class="fc-4c">
									<a href="#">Awards/ employee standards</a>
								</div>
							</div>
							<div class="flex-container">
								<div class="fc-4c">
									<div class="c-footer-wrapper-image">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/dist/assets/gfx/new-athena-swan-logo.png" />
									</div>
								</div>
								<div class="fc-4c">
									<div class="c-footer-wrapper-image">
										<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/dist/assets/gfx/rospa-gold.png" />
								</div>
								</div>
							</div>
						</div>
					 </div><!-- End Flexbox 5 columns -->

					 <!-- Flexbox 3 columns -->
					 <div class="fc-2c">
						 <div class="c-right">
							 <h6>Contact</h6>
							 <p>Wellcome Genome Campus</br>
								 Hinxton, Cambridgeshire</br>
								 CB10 1SA, UK
							 </p>
							<p>+44 (0)1223 834244</p>
						 </div>


					 </div><!-- End Flexbox 2 columns -->
		</div><!-- flex-container -->
	</footer><!-- #footer -->

<?php wp_footer(); ?>
</div><!-- site-content -->
</div><!-- site -->
</body>
</html>
